package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "PICK_LIST_ITEMS_RECEIPT")
public class FIND_BATCH implements Serializable {


    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "DOCNUM")
    public String DOCNUM;

    @ColumnInfo(name = "DOCDATE")
    public String DOCDATE;

    @ColumnInfo(name = "CARDNAME")
    public String CARDNAME;

    @ColumnInfo(name = "ITEMCODE")
    public String ITEMCODE;

    @ColumnInfo(name = "DSCRIPTION")
    public String DSCRIPTION;


    @ColumnInfo(name = "DISTNUMBER")
    public String DISTNUMBER;


    @ColumnInfo(name = "MNFDATE")
    public String MNFDATE;

    @ColumnInfo(name = "U_RETESTDATE")
    public String U_RETESTDATE;

    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

}
