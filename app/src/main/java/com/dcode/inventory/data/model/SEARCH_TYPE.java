package com.dcode.inventory.data.model;

import java.io.Serializable;

public class SEARCH_TYPE implements Serializable {
    public long PK_ID;
    public String CODE;
    public String NAME;
    public long PARENT_ID;

    public SEARCH_TYPE(long PK_ID, String CODE, String NAME, long PARENT_ID) {
        this.PK_ID = PK_ID;
        this.CODE = CODE;
        this.NAME = NAME;
        this.PARENT_ID = PARENT_ID;
    }
}
