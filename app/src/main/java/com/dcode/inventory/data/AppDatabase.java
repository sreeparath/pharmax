package com.dcode.inventory.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.dcode.inventory.data.dao.GenericDao;
import com.dcode.inventory.data.model.APP_SETTINGS;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.OBJECTS;
import com.dcode.inventory.data.model.ORTYP;
import com.dcode.inventory.data.model.FIND_BATCH;
import com.dcode.inventory.data.model.USERS;

@Database(entities = {
        USERS.class,
        OBJECTS.class,
        ORTYP.class,
        FIND_BATCH.class,
        DocType.class,
        APP_SETTINGS.class
}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract GenericDao genericDao();
}
