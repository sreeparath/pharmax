package com.dcode.inventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ORTYP")
public class ORTYP {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "ORCODE")
    public String ORCODE;

    @ColumnInfo(name = "ORNAME")
    public String ORNAME;

    @Override
    public String toString() {
        return ORNAME; // String.format("%s : %s", ORCODE, ORNAME);
    }
}
