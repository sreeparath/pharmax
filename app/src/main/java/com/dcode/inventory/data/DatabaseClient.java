package com.dcode.inventory.data;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.dcode.inventory.App;

public class DatabaseClient {

    private static AppDatabase appDatabase;
    private static DatabaseClient mInstance;

    private DatabaseClient(Context context) {
        appDatabase = Room
                .databaseBuilder(context, AppDatabase.class, "Data.db")
                .allowMainThreadQueries()
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                    }
                })
                .build();
    }

    public static synchronized DatabaseClient getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(context);
        }
        return mInstance;
    }

    public AppDatabase getAppDatabase() {
        if (!appDatabase.isOpen()) {
            appDatabase.getOpenHelper().getWritableDatabase();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.d(App.TAG, e.getMessage());
            }
        }

        return appDatabase;
    }
}
