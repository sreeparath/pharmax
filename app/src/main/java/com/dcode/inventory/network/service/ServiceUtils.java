package com.dcode.inventory.network.service;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants.DatabaseEntities;
import com.dcode.inventory.ui.dialog.CustomProgressDialog;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ServiceUtils {


    public static JsonObject createJsonObject(String paramName, String ParamType, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, String ParamTypName, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", ParamTypName);
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, long Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonElement GenerateCommonReadParams(long PK_ID) {
        JsonArray array = new JsonArray();
        JsonObject jsonObject;

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "USER_ID");
        jsonObject.addProperty("ParamType", "22");
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", App.currentUser.USER_ID);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "ID");
        jsonObject.addProperty("ParamType", "8");
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", PK_ID);
        array.add(jsonObject);
        return array;
    }

    public static class MastersDownload {

        static JsonObject getMastersRequestObject(final int masterID) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "APP.HHT_OBJECTS_MIN_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getLocationsObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.LOCLIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getOrTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.ORTYP_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject geRecTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.REC_TYPE_LIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject geReasonTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.REASON_TYPE_LIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getAuditInstructionsRequestObject() {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
//            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "DBO.HHT_AUDIT_INSTRUCT_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getMovementInstructionsRequestObject() {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
//            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "DBO.HHT_MOVEMENT_INSTRUCT_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }
    }

    public static class Batch {

        public static JsonObject getFindBatch(final String itemCode, final String batch,final String procName) {
            JsonObject jsonObject;

            jsonObject = new JsonObject();
            jsonObject.addProperty("ItemCode", itemCode);
            jsonObject.addProperty("ItemBatch", batch);
            jsonObject.addProperty("ProcName", procName);
            return jsonObject;
        }


    }



    public static class SyncActivity {
        int callCounter = 0;

        private CustomProgressDialog progressDialog;
        private boolean mIsSilentMode;
        private Context mContext;

        public SyncActivity(Context context, boolean isSilentMode) {
            this.mContext = context;
            this.mIsSilentMode = isSilentMode;
        }


        void showToast(String message) {
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }

        void showProgress(boolean isCancelable) {
            if (progressDialog != null && progressDialog.isShowing())
                return;

            progressDialog = new CustomProgressDialog(mContext, isCancelable);
            progressDialog.show();
        }

        public void updateProgressMessage(String message) {
            if (mIsSilentMode) {
                showToast(message);
                return;
            } else {
                writeProgressToUIActivity(message);
            }

/*            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.updateMessage(message);
            }*/
        }

        void writeProgressToUIActivity(String message) {
            try {

                if (TextUtils.isEmpty(message)) return;

                TextView textView;
                Activity activity = (Activity) mContext;
                if (activity != null) {
                    textView = activity.findViewById(R.id.tvMessage);
                } else {
                    return;
                }

                if (textView == null) return;

                activity.runOnUiThread(() -> {
                    textView.append("\n");
                    textView.append(message);
//                    textView.setMovementMethod(new ScrollingMovementMethod()); // no effect seen
                });

//                if (callCounter == 0 && textView.getText().toString().contains("failed")) {
//                    showToast("Something failed");
//                }

            } catch (Exception e) {
                Log.d(App.TAG, e.toString());
            }
        }

        void dismissProgress() {
            if (callCounter > 0) return;
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        }

        public void Downloads(final DatabaseEntities entities, final int PK_ID) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            switch (entities) {
                case LOCATION:
                    requestObject = MastersDownload.getLocationsObject();
                    break;
                case ORTYP:
                    requestObject = MastersDownload.getOrTypObject();
                    break;
                case RECTYP:
                    requestObject = MastersDownload.geRecTypObject();
                    break;
                case REASON_TYPE:
                    requestObject = MastersDownload.geReasonTypObject();
                    break;
                case BARCODE_PRINTERS:
                case LABEL_FORMATS:
                    requestObject = new JsonObject();
                    requestObject.addProperty("DBName", App.currentCompany.DbName);
                    break;
                default:
                    requestObject = MastersDownload.getMastersRequestObject(PK_ID);
                    break;
            }
            callCounter++;


        }




    }



    public static JsonObject ValidateUserLogin(String userName,String password) {
        JsonObject jsonObject;

        JsonArray array = new JsonArray();
        jsonObject = createJsonObject("USER_NAME", "22",userName);
        array.add(jsonObject);
        jsonObject = createJsonObject("USER_PASSWORD", "22", password);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("SPName", "SAP.VALIDATEUSER");
        jsonObject.addProperty("DBName", App.currentCompany.DbName);
        jsonObject.addProperty("USER_PASSWORD", userName);
        jsonObject.addProperty("USER_NAME", password);
        //jsonObject.add("dbparams", array);
        return jsonObject;
    }


}
