package com.dcode.inventory.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FindBatch5Adapter
        extends RecyclerView.Adapter<FindBatch5Adapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private JSONArray dataList;
    private JSONArray dataListFull;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            JSONArray filteredList = new JSONArray();

            if (constraint == null || constraint.length() == 0) {
                filteredList.put(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

//                for (BARCODE item : dataListFull) {
//                    if (item.MENU_NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
//                            item.MENU_DESC.toLowerCase().contains(filterPattern.toLowerCase())) {
//                        filteredList.add(item);
//                    }
//                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
//            dataList.clear();
//            dataList.putAll((List) results.values);
//            notifyDataSetChanged();
        }
    };

    public FindBatch5Adapter(JSONArray detList) {
        this.dataList = detList;
        //this.dataArray = dataArray;
        setHasStableIds(true);
//        grDetListFull = new ArrayList<>(detList);
    }

    public FindBatch5Adapter(Context context, JSONArray objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = objects;
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_find_batch5, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        try {
            final JSONObject objectItem = dataList.getJSONObject(position);
            Log.d("objectItem##",objectItem.toString());
            holder.text1.setText(String.valueOf(objectItem.getString("Batch Number")));
            holder.text2.setText(String.valueOf(objectItem.getString("Wharehouse")));
            holder.text3.setText(String.valueOf(objectItem.getString("Expiry Date")));
            holder.text4.setText(String.valueOf(objectItem.getString("Inhouse Retest Date")));
            holder.text5.setText(String.valueOf(objectItem.getString("QC Status")));
            holder.text6.setText(String.valueOf(objectItem.getString("InStock")));
            holder.text7.setText(String.valueOf(objectItem.getString("UOM")));
            holder.itemView.setTag(objectItem);
            holder.itemView.setOnClickListener(shortClickListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataList.length();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addItems(JSONArray  list) {
        this.dataList = list;
        this.dataListFull = list;
        //this.dataArray = dataArray;
        notifyDataSetChanged();
    }

    public JSONArray  getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView text1;
        TextView text2;
        TextView text3;
        TextView text4;
        TextView text5;
        TextView text6;
        TextView text7;
        LinearLayout borderPanel;

        RecyclerViewHolder(View view) {
            super(view);
            text1 = view.findViewById(R.id.text1);
            text2 = view.findViewById(R.id.text2);
            text3 = view.findViewById(R.id.text3);
            text4 = view.findViewById(R.id.text4);
            text5 = view.findViewById(R.id.text5);
            text6 = view.findViewById(R.id.text6);
            text7 = view.findViewById(R.id.text7);
            //borderPanel = view.findViewById(R.id.borderPanel);
        }
    }

}
