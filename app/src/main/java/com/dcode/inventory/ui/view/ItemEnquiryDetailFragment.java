package com.dcode.inventory.ui.view;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.ParcelArray;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.FIND_BATCH;
import com.dcode.inventory.ui.adapter.FindBatch3Adapter;
import com.dcode.inventory.ui.adapter.FindBatch5Adapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;

import java.util.List;
import java.util.Objects;

public class ItemEnquiryDetailFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    //PICK_HDR pl_hdr;
    FIND_BATCH picklist_item_rec;
    List<FIND_BATCH> picklist_item_recList;
    private TextInputEditText edScanCode;
    private View root;
    //private PickListItemsRecAdapter plItemsRecAdapter;
    private AutoCompleteTextView tvUnits;
    private ArrayAdapter<DocType> objectsAdapter;
    private DocType unitType;
    //private Button btnAdd;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private float rcd_qty;
    LinearLayout tableView;
    private RecyclerView recyclerView;
    private FindBatch5Adapter batchesAdapter;
    private String itcode="";
    private String batchNo="";
    private JSONArray masterData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                masterData =  ((ParcelArray) ((Bundle) getArguments()).getSerializable(AppConstants.SELECTED_OBJECT)).getArray();
            }
        }

        root = inflater.inflate(R.layout.fragment_item_details, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //loadData();
        //JSONArray array = null;
        try {
            //array = new JSONArray(masterData);

            if (masterData.length()==0) {
                showAlert("Warnig"," Error: No data received.");
                edScanCode.requestFocus();
                return;
            }
            batchesAdapter.addItems(masterData);
            batchesAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupView() {
//        edScanCode = root.findViewById(R.id.edScanCode);
//        edScanCode.setOnKeyListener((v, keyCode, event) -> {
//            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                    keyCode == KeyEvent.KEYCODE_ENTER) {
//                return onScanCodeKeyEvent();
//            }
//            return false;
//        });

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> onNextClick());

        tableView = root.findViewById(R.id.infopanel);

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        batchesAdapter = new FindBatch5Adapter(getContext(),new JSONArray(),this);
        recyclerView.setAdapter(batchesAdapter);

    }



    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //plItemsRecAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {

    }

    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {

    }


}
