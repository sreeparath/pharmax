package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppPreferences;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class SettingsFragment extends BaseFragment {
    private View root;
    private TextInputEditText edServerURL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        root = inflater.inflate(R.layout.layout_settings, container, false);

        setupView();
        return root;
    }

    private void setupView() {
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        edServerURL = root.findViewById(R.id.edServerURL);
        TextInputEditText tvDeviceID = root.findViewById(R.id.tvDeviceID);

        btnNext.setOnClickListener(v -> onSave());

        tvDeviceID.setText(AppPreferences.getValue(getContext(), AppPreferences.DEV_UNIQUE_ID, true));

        String ServiceURL = AppPreferences.getValue(getContext(), AppPreferences.SERVICE_URL, false);
        if (ServiceURL.length() <= 0)
            ServiceURL = getString(R.string.default_url);
        edServerURL.setText(ServiceURL);
    }

    private void onSave() {
        String ServiceURL = edServerURL.getText().toString();
        AppPreferences.SetValue(getContext(), AppPreferences.SERVICE_URL, ServiceURL);
        App.appSettings().setBaseUrl(ServiceURL);
        App.getNetworkClient().destroyInstance();
        getActivity().onBackPressed();
    }
}
