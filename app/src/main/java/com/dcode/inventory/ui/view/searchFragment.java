package com.dcode.inventory.ui.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.ui.adapter.ObjectsAdapter;

public class searchFragment
        extends BaseFragment {
    //        implements View.OnClickListener, SearchView.OnQueryTextListener {
    private int Master_ID;
    private int Parent_ID;
    private String filterString = "";
    private ObjectsAdapter objectsAdapter;
    private int[] fkey_intArray;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_search, container, false);

        Intent intent = getActivity().getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Master_ID = bundle.getInt(AppConstants.MASTER_ID, 0);
            Parent_ID = bundle.getInt(AppConstants.PARENT_ID, -1);

            if (bundle.containsKey(AppConstants.FILTER_KEY_INT_ARRAY)) {
                fkey_intArray = bundle.getIntArray(AppConstants.FILTER_KEY_INT_ARRAY);
            }
        }

        if (Master_ID != 0) {
//            setupRecycleView();
        } else {
            getActivity().setResult(Activity.RESULT_CANCELED, intent);
            getActivity().finish();
        }

        return view;
    }


//    @Override
//    protected int getLayoutResourceId() {
//        return R.layout.fragment_search;
//    }

//    @Override
//    protected void onViewReady(Bundle savedInstanceState) {
//        Intent intent = this.getIntent();
//        Bundle bundle = intent.getExtras();
//        if (bundle != null) {
//            Master_ID = bundle.getInt(AppConstants.MASTER_ID, 0);
//            Parent_ID = bundle.getInt(AppConstants.PARENT_ID, -1);
//
//            if (bundle.containsKey(AppConstants.FILTER_KEY_INT_ARRAY)) {
//                fkey_intArray = bundle.getIntArray(AppConstants.FILTER_KEY_INT_ARRAY);
//            }
//        }
//
//        if (Master_ID != 0) {
//            setupRecycleView();
//        } else {
//            setResult(RESULT_CANCELED, intent);
//            finish();
//        }
//    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar, menu);

        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        objectsAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();

        OBJECTS objectSearch = (OBJECTS) view.getTag();
        bundle.putInt(AppConstants.SELECTED_ID, objectSearch.OBJECT_ID);
        bundle.putString(AppConstants.SELECTED_CODE, objectSearch.OBJECT_CODE);
        bundle.putString(AppConstants.SELECTED_NAME, objectSearch.OBJECT_NAME);
        bundle.putInt(AppConstants.PARENT_ID, objectSearch.PARENT_OBJECT_ID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, objectSearch);

        Intent intent = this.getIntent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finishAndRemoveTask();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finishAndRemoveTask();
    }

    private void setupRecycleView() {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);

        objectsAdapter = new ObjectsAdapter(searchFragment.this, new ArrayList<>(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

*//*        switch (Master_ID) {
            case AppConstants.LOCATION:
                if (fkey_intArray != null && fkey_intArray.length != 3) {
                    showToast("Invalid filter received");
                } else {
                    objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getLocations(fkey_intArray[0], fkey_intArray[1], fkey_intArray[2]));
                }
                break;
            case AppConstants.AUDIT_INSTRUCTIONS:
                objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getAuditInstructions());
                break;

            case AppConstants.MOVEMENT_INSTRUCTIONS:
                objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getMovementInstructions());
                break;
            default:
                objectsAdapter.addItems(App.getDatabaseClient().getAppDatabase().genericDao().getMastersByID(Master_ID, Parent_ID));
                break;
        }*//*

        recyclerView.setAdapter(objectsAdapter);
    }
*/
}
