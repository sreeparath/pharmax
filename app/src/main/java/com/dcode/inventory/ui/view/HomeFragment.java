package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import com.dcode.inventory.R;

public class HomeFragment
        extends BaseFragment {

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        root = inflater.inflate(R.layout.fragment_home, container, false);

        TextView tv_bg_goods_receipt = root.findViewById(R.id.tv_bg_goods_receipt);
        tv_bg_goods_receipt.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_find_batch));


        TextView tv_bg_wo = root.findViewById(R.id.tv_bg_wo);
        tv_bg_wo.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_find_batch_dynamic));
        tv_bg_wo.setVisibility(View.GONE);

        TextView tv_bg_wo2 = root.findViewById(R.id.tv_bg_wo2);
        tv_bg_wo2.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_find_batch_dynamic3));
        tv_bg_wo2.setVisibility(View.GONE);

        LinearLayout firstPanel = root.findViewById(R.id.firstPanel);
        firstPanel.setVisibility(View.GONE);

        TextView tv_item_batch = root.findViewById(R.id.tv_item_batch);
        tv_item_batch.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_item_batch_header));

        TextView tv_item = root.findViewById(R.id.tv_item);
        tv_item.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_item_header));

        TextView tv_bg_settings = root.findViewById(R.id.tv_bg_settings);
        tv_bg_settings.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_settings));



        TextView tv_bg_logout = root.findViewById(R.id.tv_bg_logout);
        tv_bg_logout.setOnClickListener(v -> OnLogoutClick());

        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void OnLogoutClick() {
        ((MainActivity) requireActivity()).SignOut();
    }
}
