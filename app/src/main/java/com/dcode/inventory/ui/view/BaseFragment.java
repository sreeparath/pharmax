package com.dcode.inventory.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.ui.dialog.CustomProgressDialog;

public abstract class BaseFragment extends Fragment {
    protected int ModuleID;
    //    protected int selectedID;
//    protected String selectedCode;
//    protected String selectedName;
    private CustomProgressDialog progressDialog;
    private AlertDialog alertDialog;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

//        if (resultCode != Activity.RESULT_OK) return;
//
//        selectedID = intent.hasExtra(AppConstants.SELECTED_ID) ? intent.getIntExtra(AppConstants.SELECTED_ID, -1) : -1;
//        selectedCode = intent.hasExtra(AppConstants.SELECTED_CODE) ? intent.getStringExtra(AppConstants.SELECTED_CODE) : "";
//        selectedName = intent.hasExtra(AppConstants.SELECTED_NAME) ? intent.getStringExtra(AppConstants.SELECTED_NAME) : "";

    }

    protected void showToast(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    protected void showToastLong(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    protected void showAlert(final String title, final String message) {
        if (alertDialog != null && alertDialog.isShowing())
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        alertDialog = builder
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> {
                })
                .create();
        alertDialog.show();
        shortVibration();
    }

//    protected void OnLookupClick(int master_id, int parent_id, @Nullable int[] array) {
//        Intent intent = new Intent(getActivity(), searchFragment.class);
//        intent.putExtra(AppConstants.MASTER_ID, master_id);
//        intent.putExtra(AppConstants.PARENT_ID, parent_id);
//
//        if (array != null) {
//            intent.putExtra(AppConstants.FILTER_KEY_INT_ARRAY, array);
//        }
//        startActivityForResult(intent, master_id);
//    }

    protected void showProgress(boolean isCancelable) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new CustomProgressDialog(getContext(), isCancelable);
        progressDialog.show();
    }

    void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    protected void shortVibration() {
        Vibrator vibe = (Vibrator) this.getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (vibe != null && vibe.hasVibrator()) vibe.vibrate(10);
    }

    protected void OnLookupClick(String title, int master_id, int parent_id) {
//        Intent intent = new Intent(getActivity(), SearchActivity.class);
//        intent.putExtra(String.valueOf(AppConstants.TITLE), title);
//        intent.putExtra(AppConstants.MASTER_ID, master_id);
//        intent.putExtra(AppConstants.PARENT_ID, parent_id);
//
//        startActivityForResult(intent, master_id);
//        ((BaseActivity) requireActivity()).OnLookupClick(master_id, parent_id, null, title);
    }
}
