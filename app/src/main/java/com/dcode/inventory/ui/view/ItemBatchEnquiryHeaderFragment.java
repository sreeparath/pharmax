package com.dcode.inventory.ui.view;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Parcel;
import com.dcode.inventory.common.ParcelArray;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.FIND_BATCH;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ItemBatchEnquiryHeaderFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    //PICK_HDR pl_hdr;
    FIND_BATCH picklist_item_rec;
    List<FIND_BATCH> picklist_item_recList;
    private TextInputEditText edScanCode;
    private View root;
    //private PickListItemsRecAdapter plItemsRecAdapter;
    private AutoCompleteTextView tvUnits;
    private ArrayAdapter<DocType> objectsAdapter;
    private DocType unitType;
    //private Button btnAdd;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private float rcd_qty;
    LinearLayout tableView;
    String itcode = "";
    String batchNo = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
               // pl_hdr = (PICK_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_item_batch_enquiry_header, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //loadData();

    }

    private void setupView() {
        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                return onScanCodeKeyEvent();
            }
            return false;
        });

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> onNextClick());

        MaterialButton btnView = root.findViewById(R.id.btnView);
        btnView.setOnClickListener(v -> onAddClick());

        tableView = root.findViewById(R.id.infopanel);

    }

    private void createDataFields(String fieldName,String value){
        TextInputLayout emailTextInputLayout = new TextInputLayout(getContext(), null, R.style.AppTheme);
        emailTextInputLayout.setHint(fieldName);
        emailTextInputLayout.setBoxStrokeColorStateList(AppCompatResources.getColorStateList(getContext(),R.color.color_green_light));
        TextInputEditText edtEmail = new TextInputEditText(emailTextInputLayout.getContext());
        edtEmail.setText(value);
        edtEmail.setFocusable(false);
        emailTextInputLayout.addView(edtEmail);
        tableView.addView(emailTextInputLayout);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //plItemsRecAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();
//
//        if (ls_items == null) {
//            return;
//        }

        //NavigateToItemDetails(ls_items, "");
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if(!(itcode!=null && itcode.length()>0 ) && !(batchNo!=null && batchNo.length()>0 )){
            showAlert("Error","No item found!");
            return;
        }

        findBatch3(itcode, batchNo);

    }





    private void ParseBarCode(String scanCode) {
        resetUI();
        if (scanCode.length() <= 0  ) {
            return;
        }
        if(scanCode.length() < 6){
            showAlert("Error","Invalid Item");
            return;
        }
        itcode = scanCode.substring(0,5).toString();
        batchNo = scanCode.substring(5,scanCode.length()).toString();
        //edItemCode.setText(itcode);
        //edBatchNo.setText(batchNo);
        findBatch2(itcode,batchNo);
        edScanCode.setText("");
        edScanCode.requestFocus();
    }

    private void resetUI(){
        itcode ="";
        batchNo="";
//        edItemCode.setText("");
//        edBatchNo.setText("");
//        edMafDate.setText("");
//        edExpdate.setText("");
//        edDocDt.setText("");
//        edDocNo.setText("");
//        edOrdNo.setText("");
//        edDesc.setText("");
//        edDist.setText("");
//        edRetDt.setText("");
    }

    private void fillBatch(FIND_BATCH find_batch){
//        edMafDate.setText(find_batch.MNFDATE);
//        edExpdate.setText(find_batch.EXPDATE);
//        edDocDt.setText(find_batch.DOCDATE);
//        edDocNo.setText(find_batch.DOCNUM);
//        edOrdNo.setText(find_batch.CARDNAME);
//        edDesc.setText(find_batch.DSCRIPTION);
//        edDist.setText(find_batch.DISTNUMBER);
//        edRetDt.setText(find_batch.U_RETESTDATE);
    }

    private void findBatch2(final String itemCode, final String batch) {
        if(!(itemCode!=null && itemCode.length()>0 ) && !(batch!=null && batch.length()>0 )){
            showAlert("Error","No item found!");
            return;
        }

        JsonObject requestObject =  ServiceUtils.Batch.getFindBatch(itemCode, batch,"FINDBATCH2");

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {

                        Log.d("data##",xmlDoc.toString());
                        tableView.removeAllViews();
                        JSONArray array = new  JSONArray(xmlDoc);
                        if (array.length()==0) {
                            showAlert("Warnig"," Error: No data received.");
                            edScanCode.requestFocus();
                            return;
                        }
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject objects = array.getJSONObject(i);
                            Iterator key = objects.keys();
                            while (key.hasNext()) {
                                String k = key.next().toString();
                                createDataFields( k,objects.getString(k));
                            }
                            edScanCode.requestFocus();
                        }

                    } catch (Exception e) {
                        resetUI();
                        Log.d(App.TAG, e.toString());
                        showAlert("Error",e.toString());
                    }
                } else {
                    resetUI();
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error",msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                resetUI();
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error",msg);
            }
        });
    }

    private void findBatch3(final String itemCode, final String batch) {

        JsonObject requestObject =  ServiceUtils.Batch.getFindBatch(itemCode, batch,"FINDBATCH3");

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        Log.d("data##",xmlDoc.toString());
                        JSONArray array = new  JSONArray(xmlDoc);
                        if (array.length()==0) {
                            showAlert("Warnig"," Error: No data received.");
                            edScanCode.requestFocus();
                            return;
                        }
                        Bundle bundle = new Bundle();
                        ParcelArray p = new ParcelArray(array);
                        bundle.putSerializable(AppConstants.SELECTED_OBJECT, p);
                        ((MainActivity) requireActivity()).NavigateToFragment(R.id.action_nav_item_batch_detail, bundle);
                    } catch (Exception e) {
                        resetUI();
                        Log.d(App.TAG, e.toString());
                        showAlert("Error",e.toString());
                    }
                } else {
                    resetUI();
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error",msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                resetUI();
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error",msg);
            }
        });
    }

}
