package com.dcode.inventory;

import android.app.Application;
import android.content.Context;

import com.dcode.inventory.common.AppPreferences;
import com.dcode.inventory.common.AppVariables;
import com.dcode.inventory.data.DatabaseClient;
import com.dcode.inventory.data.MySettings;
import com.dcode.inventory.data.model.COMPANY;
import com.dcode.inventory.data.model.USERS;
import com.dcode.inventory.network.DeviceNetworkInfo;
import com.dcode.inventory.network.NetworkClient;
import com.facebook.stetho.Stetho;
//import com.facebook.stetho.Stetho;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

public class App extends Application {
    public static String TAG = "ERR_TAG";
    public static String DeviceID = "12345";
    public static String DeviceIP = "10.0.2.1";
    public static String PrinterMAcId = "";  //00:06:66:65:18:12
    public static String base64CompanyLogoPng = "";
    public static String PrinterCode = "PB51";
    public static String jsonCmdAttribStr = "";
    public static USERS currentUser;
    public static COMPANY currentCompany;
    public static long gr_hdr_id = -1;
    public static long ls_hdr_id = -1;
    public static long pl_hdr_id = -1;
    public static long sr_hdr_id = -1;
    public static String BarCodeSeparator = "_";
    public static String BarCodeSeparatorDollor = "\\$";


    private static Context context;

    public static NetworkClient getNetworkClient() {
        return NetworkClient.getInstance();
    }

    public static DatabaseClient getDatabaseClient() {
        return DatabaseClient.getInstance(context);
    }

    public static MySettings appSettings() {
        return MySettings.getInstance();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("font/dubai-regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        DeviceIP = DeviceNetworkInfo.getIPAddress(true);
        DeviceID = AppPreferences.getValue(context, AppPreferences.DEV_UNIQUE_ID, true);
        PrinterMAcId = AppPreferences.getValue(context, AppPreferences.PRINTER_MAC_ID, false);

        String ServiceURL = AppPreferences.getValue(context, AppPreferences.SERVICE_URL, false);
        if (ServiceURL == null || ServiceURL.length() <= 0) {
            appSettings().setBaseUrl(getString(R.string.default_url));
        } else {
            appSettings().setBaseUrl(ServiceURL);
        }
        //appSettings().setBaseUrl("https://f64dde5c-e3ec-4c5d-a5a7-cad33884a409.mock.pstmn.io");
        //appSettings().setBaseUrl("https://72249e95-a329-4473-8579-7f6ec0098401.mock.pstmn.io");

        //appSettings().setBaseUrl("https://ec576afe-67cd-41f0-a0a1-fd5a810748b0.mock.pstmn.io");
        //appSettings().setBaseUrl("https://a6a1192e-f485-4a07-9cd3-94aa1227ae1a.mock.pstmn.io");

        //new AppVariables(context);

        Stetho.initializeWithDefaults(context);
    }

    public static void resetApp(){
        App.currentCompany = null;
        App.currentUser = null;
    }




}
