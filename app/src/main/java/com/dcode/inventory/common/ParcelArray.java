package com.dcode.inventory.common;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class ParcelArray implements Serializable {
    private JSONArray array;

    public ParcelArray(JSONArray array) {
        this.array = array;
    }

    public JSONArray getArray() {
        return array;
    }

}